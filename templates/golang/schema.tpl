import { Prop, Schema as SchemaNest, SchemaFactory } from "@nestjs/mongoose";
import {Document, Schema} from "mongoose";
import {IsNotEmpty} from "class-validator";
{%- if entity.IsPaginate %}
import * as mongoosePaginate from 'mongoose-paginate-v2';
{%- endif %}

export class {{entity.Name|capfirst}}Payload{
{%- for field in entity.Fields -%}
{%- if field.IsRequired %}
  @IsNotEmpty()
  {{field.Name}}:{{ field.FieldType }};
{%- else %}
  {{field.Name}}:{{ field.FieldType }};
{%- endif %}
{% endfor %}
}

@SchemaNest({
    timestamps: true
})
export class {{entity.Name|capfirst}} extends Document{
{% for field in entity.Fields %}
  @Prop()
  {{field.Name}}:{{field.FieldType}};
{% endfor %}
}

export const {{entity.Name|capfirst}}Schema = SchemaFactory.createForClass({{entity.Name|capfirst}});
{% if entity.IsPaginate %}
{{entity.Name|capfirst}}Schema.plugin(mongoosePaginate);
{% endif %}
