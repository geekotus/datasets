package payload

import (
    "go.mongodb.org/mongo-driver/bson/primitive"
    "time"
)

type {{entity.Name|capfirst}}Payload struct {
    ID primitive.ObjectID  `json:"id,omitempty"`
{%- for field in entity.Fields %}
    {{field.Name|capfirst}} {% if field.IsArray %}[]{%- endif %}{{ field.Type }} `json:"{{field.Name}}"`
{%- endfor %}
}
