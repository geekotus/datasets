package repository

import (
	"{{dictionary.Project.Module}}/domain"
	"{{dictionary.Project.Module}}/errs"
	"{{dictionary.Project.Module}}/logger"
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type {{entity.Name|capfirst}}RepositoryDb struct {
	client *mongo.Client
}

func (d {{entity.Name|capfirst}}RepositoryDb) Create(user domain.{{entity.Name|capfirst}}) (*domain.{{entity.Name|capfirst}},*errs.AppError){
	collection := d.client.Database("{{dictionary.Project.Database}}").Collection("{{entity.Tablename}}")

	userCreated, err := collection.InsertOne(context.TODO(),user)
	if err != nil {
		logger.Error("Error ao tentar gravar o {{entity.Title}}: " + err.Error())
		return nil, errs.NewUnexpectedError("Unexpected database error")
	}
	user.ID = userCreated.InsertedID.(primitive.ObjectID)
	return &user,nil
}

func (d {{entity.Name|capfirst}}RepositoryDb) Update(id primitive.ObjectID, user domain.{{entity.Name|capfirst}}) (*domain.{{entity.Name|capfirst}},*errs.AppError){
	collection := d.client.Database("{{dictionary.Project.Database}}").Collection("{{entity.Tablename}}")

	filter := bson.D{{quoteIn}}"_id", id{{quoteOut}}

	return &userUpdate,nil
}


func New{{entity.Name|capfirst}}RepositoryDb(client *mongo.Client) {{entity.Name|capfirst}}RepositoryDb {
	return {{entity.Name|capfirst}}RepositoryDb{client}
}
