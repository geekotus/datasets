package domain

import (
    "go.mongodb.org/mongo-driver/bson/primitive"
    "time"
)

type {{entity.Name|capfirst}} struct {
    ID primitive.ObjectID  `json:"id,omitempty" bson:"_id,omitempty"`
{%- for field in entity.Fields %}
    {{field.Name|capfirst}} {% if field.IsArray %}[]{%- endif %}{{ field.Type }} `json:"{{field.Name}}" bson:"{{field.Name}},omitempty"`
{%- endfor %}
    CreatedAt    time.Time `json:"createdAt"  bson:"createdAt,omitempty"`
    UpdatedAt    time.Time `json:"updatedAt"  bson:"updatedAt,omitempty"`
}
