package dto

import (
    "{{dictionary.Project.Module}}/errs"
)


type {{entity.Name|capfirst}}Dto struct {
{% for field in entity.Fields %}
    {{field.Name|capfirst}} {% if field.IsArray %}[]{%- endif %}{{ field.Type }} `json:"{{field.Name}}"`
{%- endfor %}
}
{%- if entity.IsRequired %}

func (v {{entity.Name|capfirst}}Dto) Validate() *errs.AppError {
{%- for field in entity.Fields -%}
{%- if field.Validate.IsRequired %}
    if v.{{field.Name}} == "" {
        return errs.NewValidationError("{{field.Validate.Message}}")
    }
{%- endif -%}
{%- if field.Validate.Rule == "email" %}
    if error := checkmail.ValidateFormat(v.Email); error != nil {
        return errs.NewValidationError("O e-mail inserido é inválido")
    }
{%- endif -%}
{%- endfor %}
    return nil
}
{% endif -%}