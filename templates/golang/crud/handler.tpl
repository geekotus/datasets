package handler

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type {{entity.Name|capfirst}}Handler struct {
	service service.{{entity.Name|capfirst}}Service
}

func (h *{{entity.Name|capfirst}}Handler) New{{entity.Name|capfirst}}(w http.ResponseWriter, r *http.Request){
	var {{entity.Name}} {{entity.Name}}.{{entity.Name|capfirst}}Dto

	err:= json.NewDecoder(r.Body).Decode(&{{entity.Name}})

	if err != nil {
		web.WriteResponse(w, http.StatusBadRequest, err.Error())
	} else {
		result,err := h.service.Create({{entity.Name}})

		if err !=nil {
			web.WriteResponse(w, err.Code, err.AsMessage())
		}else{
			web.WriteResponse(w, http.StatusCreated, result)
		}
	}
}

func (h *{{entity.Name|capfirst}}Handler) GetAll{{entity.Name|capfirst}}s(w http.ResponseWriter, r *http.Request) {

	{{entity.Name}}s, err := h.service.GetAll()

	if err != nil {
		web.WriteResponse(w, err.Code, err.AsMessage())
	} else {
		web.WriteResponse(w, http.StatusCreated, {{entity.Name}}s)
	}
}

func (h *{{entity.Name|capfirst}}Handler) Get{{entity.Name|capfirst}}ById(w http.ResponseWriter, r *http.Request) {

	param := mux.Vars(r)["id"]
	id, err := primitive.ObjectIDFromHex(param)
	if err != nil {
		web.WriteResponse(w, http.StatusUnprocessableEntity,err.Error())
	}

	{{entity.Name}}, appError := h.service.GetById(id)

	if appError != nil {
		web.WriteResponse(w, appError.Code, appError.AsMessage())
	} else {
		web.WriteResponse(w, http.StatusCreated, {{entity.Name}})
	}
}

func (h *{{entity.Name|capfirst}}Handler) Updated{{entity.Name|capfirst}}(w http.ResponseWriter, r *http.Request) {
	param := mux.Vars(r)["id"]
	id, err := primitive.ObjectIDFromHex(param)
	if err != nil {
		web.WriteResponse(w, http.StatusUnprocessableEntity,err.Error())
	}

	var {{entity.Name}} {{entity.Name}}.{{entity.Name|capfirst}}EditDto
	err = json.NewDecoder(r.Body).Decode(&{{entity.Name}})
	if err != nil {
		web.WriteResponse(w, http.StatusBadRequest, err.Error())
	} else {
		u,err := h.service.Update(id,{{entity.Name}})

		if err !=nil {
			web.WriteResponse(w, err.Code, err.AsMessage())
		}else{
			web.WriteResponse(w, http.StatusCreated, u)
		}
	}
}

func (h *{{entity.Name|capfirst}}Handler) Delete{{entity.Name|capfirst}}(w http.ResponseWriter, r *http.Request) {

	param := mux.Vars(r)["id"]
	id, err := primitive.ObjectIDFromHex(param)

	if err != nil {
		web.WriteResponse(w, http.StatusBadRequest, err.Error())
	} else {
		err := h.service.Delete(id)

		if err !=nil {
			web.WriteResponse(w, err.Code, err.AsMessage())
		}else{
			web.WriteResponse(w, http.StatusOK, "{{entity.Title|capfirst}} excluído com sucesso!")
		}
	}
}